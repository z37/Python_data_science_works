README File
- @name: Python_data_science_works.
- @author: z37.
- @detail: The exercises done are part of a compendium of  notes using machine learning with python. The information presented does not represent any official information only educational.

## Description
This notebook consists of 5 sections in which various topics are addressed.

### Section 1
1.1.- Knn (K nearest neighbor)
### Section 2
* 2.1.- Data exploration lore 
* 2.2.- Visual data regression
* 2.3.- Visual data lore
### Section 3
* 3.1.- Data preparation lore
* 3.2.- Data preparation
### Section 4
* 4.1.- Applied linear regression
* 4.2.- Linear regression lore
* 4.3.- Classification
* 4.4.- Introduction to linear regression.
### Section 5
* 5.1.- Decision tree lore
* 5.2.- Support vector machine lore
* 5-3.- Naive bayes lore

## Usage
```
written in pyhton 3.6
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.
